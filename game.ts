import Phaser from 'phaser'
import * as R from 'ramda'

interface Options {
    canvas: HTMLCanvasElement
    window: Window
}

const resize = (options: Options, game: Phaser.Game) => {
    const windowWidth = options.window.innerWidth
    const windowHeight = options.window.innerHeight
    const windowRatio = windowWidth / windowHeight
    const gameRatio = (game.config.width as number) / (game.config.height as number)

    if (windowRatio < gameRatio) {
        options.canvas.style.width = windowWidth + 'px'
        options.canvas.style.height = (windowWidth / gameRatio) + 'px'
    } else {
        options.canvas.style.width = (windowHeight * gameRatio) + 'px'
        options.canvas.style.height = windowHeight + 'px'
    }
}

export const setup = (options: Options) => {
    const config = {
        type: Phaser.CANVAS,
        width: 800,
        height: 600,
        pixelArt: true,
        canvas: options.canvas,
        physics: {
            default: 'arcade',
        },
        scene: {
            preload() {
                preload(this, options)
            },
            create() {
                create(this, options)
                // const cam = this.cameras.main
                // cam.setBounds(0, 0, 800, 600)
                // cam.setZoom(1)
                // cam.centerOn(0, 0)
                // cam.pan(200, 300, 2000)
                // cam.zoomTo(4, 2000)
                // const particles = this.add.particles('red')
            }
        }
    } as Phaser.Types.Core.GameConfig

    const game = new Phaser.Game(config)
    resize(options, game)
}

const preload = (ctx: Phaser.Scene, _options: Options) => {
    // ctx.load.setBaseURL('http://labs.phaser.io')
    ctx.load.image('tiles', '/desert.png')
}

const of = (n: number) => [...Array(n)]

interface Tile {
    name: string
    tilesetId: number
}

interface Resource extends Tile {
    amount: number
}

type Terrain = (Resource | Tile)[]

const tile = (tile: Tile) => ({
    ...tile
})

const resource = (resource: Resource) => ({
    ...resource
})

const SAND = tile({ tilesetId: 29, name: 'Sand' })
const CACTUS = tile({ tilesetId: 30, name: 'Cactii' })
const STREET = tile({ tilesetId: 9, name: 'Street' })
const RAIL = tile({ tilesetId: 49, name: 'Rail' })
const SELECTION = tile({ tilesetId: 51, name: 'Selection' })
const ROCK = tile({ tilesetId: 31, name: 'Rock' })
const GRAYBUSH = tile({ tilesetId: 38, name: 'Gray Bush' })
const GRAYBUSHSMALL = tile({ tilesetId: 46, name: 'Small Gray Bush' })

enum Orientation {
    NORTH,
    EAST,
    SOUTH,
    WEST,
}

const create = (ctx: Phaser.Scene, options: Options) => {
    const map = ctx.make.tilemap({
        width: 40,
        height: 40,
        tileWidth: 32,
        tileHeight: 32,
    })
    const tiles = map.addTilesetImage('tiles', undefined, 32, 32, 1, 1)
    const terrainLayer = map.createBlankLayer('terrainLayer', tiles)
    const userLayer = map.createBlankLayer('userLayer', tiles)
    const selectionLayer = map.createBlankLayer('selectionLayer', tiles)
    const placementLayer = map.createBlankLayer('placementLayer', tiles)
    const generateTerrain = (): Tile | Resource => {
        const rand = Math.random()
        if (rand < 0.025) return resource({ ...ROCK, amount: 10 + Math.random() * 20 | 0 })
        if (rand < 0.050) return resource({ ...GRAYBUSH, amount: 2 + Math.random() * 8 | 0 })
        if (rand < 0.075) return resource({ ...GRAYBUSHSMALL, amount: 2 + Math.random() * 3 | 0 })
        return tile({ ...SAND })
    }
    const terrain = R.map(() => R.map(generateTerrain, of(50)), of(50))
    const user = R.map(() => R.map(() => ({}), of(50)), of(50))
    const getTilesFromTerrain = R.map<Terrain, number[]>(R.map(t => t.tilesetId))
    terrainLayer.putTilesAt(getTilesFromTerrain(terrain), 0, 0)
    const text = ctx.add.text(10, 10, '', {
        fontSize: '16px',
        fontFamily: 'Arial',
        color: 'red'
    })
    let currentPlacementOrientation = Orientation.WEST
    const getRotationFromOrientation = (currentOrientation: Orientation) => ({
        [Orientation.NORTH]: 0,
        [Orientation.EAST]: 11,
        [Orientation.SOUTH]: 22,
        [Orientation.WEST]: 33,
    })[currentOrientation]
    const getNextOrientation = (currentOrientation: Orientation) => ({
        [Orientation.NORTH]: Orientation.WEST,
        [Orientation.EAST]: Orientation.NORTH,
        [Orientation.SOUTH]: Orientation.EAST,
        [Orientation.WEST]: Orientation.SOUTH,
    })[currentOrientation]
    ctx.input.keyboard.on('keyup', (event: Phaser.Input.Keyboard.Key & KeyboardEvent) => {
        const actions = {
            KeyR: () => {
                currentPlacementOrientation = getNextOrientation(currentPlacementOrientation)
                placementLayer.putTileAtWorldXY(RAIL.tilesetId, ...lastSelection).setAlpha(0.4).rotation = getRotationFromOrientation(currentPlacementOrientation)
            }
        }
        const code = event.code
        actions[code] && actions[code]()
    })
    ctx.input.on('pointerdown', (event: Phaser.Input.Pointer) => {
        const selectedTerrainTile = terrainLayer.getTileAtWorldXY(event.worldX, event.worldY)
        if (terrainLayer.getTileAtWorldXY(event.worldX, event.worldY).index != SAND.tilesetId) return
        const selectedUserTile = userLayer.getTileAtWorldXY(event.worldX, event.worldY)
        if (selectedUserTile && selectedUserTile.index == RAIL.tilesetId) return userLayer.removeTileAtWorldXY(event.worldX, event.worldY)
        userLayer.putTileAtWorldXY(RAIL.tilesetId, event.worldX, event.worldY).rotation = getRotationFromOrientation(currentPlacementOrientation)
        user[selectedTerrainTile.y][selectedTerrainTile.x] = RAIL
        // const tile = userLayer.findTile(t => t.index == STREET)
    })
    let lastSelection = [0, 0] as [number, number]
    ctx.input.on('pointermove', (event: Phaser.Input.Pointer) => {
        placementLayer.removeTileAtWorldXY(...lastSelection)
        selectionLayer.removeTileAtWorldXY(...lastSelection)
        selectionLayer.putTileAtWorldXY(SELECTION.tilesetId, event.worldX, event.worldY)

        lastSelection = [event.worldX, event.worldY]
        // selectionLayer.removeTile
        const selectedTerrainTile = terrainLayer.getTileAtWorldXY(event.worldX, event.worldY)
        const t = terrain[selectedTerrainTile.y][selectedTerrainTile.x]
        const u = user[selectedTerrainTile.y][selectedTerrainTile.x]
        if (!u.name) placementLayer.putTileAtWorldXY(RAIL.tilesetId, event.worldX, event.worldY).setAlpha(0.4).rotation = getRotationFromOrientation(currentPlacementOrientation)
        text.text = u.name
            ? `${u.name}`
            : `${t.name} ${'amount' in t ? ', amount: ' + t.amount : ''}`
        // console.log(selectedTerrainTile)
        // console.log(terrain[selectedTerrainTile.y][selectedTerrainTile.x])
        // if (terrainLayer.getTileAtWorldXY(event.worldX, event.worldY).index != SAND) return
        // const selectedUserTile = userLayer.getTileAtWorldXY(event.worldX, event.worldY)
        // if (selectedUserTile && selectedUserTile.index == STREET) return userLayer.removeTileAtWorldXY(event.worldX, event.worldY)
        // userLayer.putTileAtWorldXY(STREET, event.worldX, event.worldY)
        // const tile = userLayer.findTile(t => t.index == STREET)
    })
}

const update = (ctx: Phaser.Scene) => {
}
