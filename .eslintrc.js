module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    extends: [
        '@nuxtjs/eslint-config-typescript',
        'plugin:nuxt/recommended'
    ],
    plugins: [
    ],
    // add your custom rules here
    rules: {
        indent: [0, 4],
        'vue/html-indent': [0, 4],
        curly: 0,
        'comma-dangle': 0,
        eqeqeq: 0,
        'space-before-function-paren': 0,
    }
}
